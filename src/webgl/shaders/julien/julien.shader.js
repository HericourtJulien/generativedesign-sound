import glslify from 'glslify';
import vertexShaderSource from './julien.vert';
import fragmentShaderSource from './julien.frag';

// TODO npm script to automate the creation of new shaders

const vertexShader = glslify(vertexShaderSource);
const fragmentShader = glslify(fragmentShaderSource);

export { vertexShader, fragmentShader };
