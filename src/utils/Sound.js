import sono from 'sono';
import 'sono/effects';

class Sound {
  constructor() {
    this.sound = sono
      .create({
        // url: ['static/sounds/zoom-zoom.mp3'],
        url: ['static/sounds/floxytek.mp3'],
        loop: true
      })
      .play();

    this.analyse = this.sound.effects.add(
      sono.analyser({
        fftSize: 256,
        smoothing: 0.7
      })
    );
  }

  getFrequencies() {
    return this.analyse.getFrequencies();
  }
}

export default new Sound();
