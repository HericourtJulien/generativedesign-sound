import { Object3D } from 'three';

class {{name}} extends Object3D {
  constructor() {
    super();
  }

  update(dt = 0, time = 0) {}
}

export default {{name}};
