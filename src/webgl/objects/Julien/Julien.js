import { Object3D, BoxGeometry, ShaderMaterial, Mesh, Color, TextureLoader } from 'three';
import * as THREE from 'three';
import { canvas, webgl, gui } from 'webgl';
import { vertexShader, fragmentShader } from 'shaders/julien/julien.shader';

class Julien extends Object3D {
  constructor() {
    super();


    // uniform float time;
    // uniform float contrast;
    // uniform float distortion;
    // uniform float speed;
    // uniform vec3 color;
    // uniform float brightness;

    const loader = new THREE.TextureLoader();
    loader.load('../../../assets/thumb_chromatic-noise.png', (texture) => {
      console.log('texture', texture);
    }, undefined, (err) => {
      console.log(err);
    });

    const uniforms = {
      speed: { value: 0.2 },
      color: { value: new Color('rgb(255,255,255)') },
      brightness: { value: 0.1 },
      time: { value: 1.0 },
      contrast: { value: 1.5 },
      distortion: { value: 2.0 },
      noiseImage: { value: new THREE.TextureLoader().load('/assets/thumb_chromatic-noise.png') }
    };

    const geometry = new BoxGeometry(50, 50, 50);
    this.material = new ShaderMaterial({
      uniforms: uniforms,
      vertexShader,
      fragmentShader
    });

    const mesh = new Mesh(geometry, this.material);

    this.add(mesh);

    // attach dat.gui stuff here as usual
    const folder = gui.addFolder('Julien');

    // this.settings = {
    //   rotationSpeed: 0.5,
    //   pulseSpeed: 1.5,
    //   colorA: this.material.uniforms.colorA.value.getStyle(),
    //   colorB: this.material.uniforms.colorB.value.getStyle()
    // };

    const update = () => {
      // this.material.uniforms.colorA.value.setStyle(this.settings.colorA);
      // this.material.uniforms.colorB.value.setStyle(this.settings.colorB);
    };

    // folder.add(this.settings, 'rotationSpeed', 0, 10.0);
    // folder.add(this.settings, 'pulseSpeed', 0, 6.0);
    // folder.addColor(this.settings, 'colorA').onChange(update);
    // folder.addColor(this.settings, 'colorB').onChange(update);
    // folder.open();
  }

  update(dt = 0, time = 0) {
    // this.material.uniforms.time.value = time * this.settings.pulseSpeed;
    //
    // this.rotation.x += dt * this.settings.rotationSpeed;
    // this.rotation.y += dt * this.settings.rotationSpeed;
  }
}

export default Julien;
